msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 17:10+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/banners/index.tags:7
msgid "Download"
msgstr ""

#: ../../english/banners/index.tags:11
msgid "Old banner ads"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:10
msgid "Working"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:20
msgid "sarge"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:30
msgid "sarge (broken)"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:40
msgid "Booting"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:50
msgid "Building"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:56
msgid "Not yet"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:59
msgid "No kernel"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:62
msgid "No images"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:65
msgid "<void id=\"d-i\" />Unknown"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:68
msgid "Unavailable"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr ""

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
msgid "More information"
msgstr ""

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr ""

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr ""

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr ""

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr ""

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr ""

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr ""

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr ""

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr ""

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr ""

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr ""

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr ""

#: ../../english/misc/merchandise.def:8
msgid "Products"
msgstr ""

#: ../../english/misc/merchandise.def:11
msgid "T-shirts"
msgstr ""

#: ../../english/misc/merchandise.def:14
msgid "hats"
msgstr ""

#: ../../english/misc/merchandise.def:17
msgid "stickers"
msgstr ""

#: ../../english/misc/merchandise.def:20
msgid "mugs"
msgstr ""

#: ../../english/misc/merchandise.def:23
msgid "other clothing"
msgstr ""

#: ../../english/misc/merchandise.def:26
msgid "polo shirts"
msgstr ""

#: ../../english/misc/merchandise.def:29
msgid "frisbees"
msgstr ""

#: ../../english/misc/merchandise.def:32
msgid "mouse pads"
msgstr ""

#: ../../english/misc/merchandise.def:35
msgid "badges"
msgstr ""

#: ../../english/misc/merchandise.def:38
msgid "basketball goals"
msgstr ""

#: ../../english/misc/merchandise.def:42
msgid "earrings"
msgstr ""

#: ../../english/misc/merchandise.def:45
msgid "suitcases"
msgstr ""

#: ../../english/misc/merchandise.def:48
msgid "umbrellas"
msgstr ""

#: ../../english/misc/merchandise.def:51
msgid "pillowcases"
msgstr ""

#: ../../english/misc/merchandise.def:54
msgid "keychains"
msgstr ""

#: ../../english/misc/merchandise.def:57
msgid "Swiss army knives"
msgstr ""

#: ../../english/misc/merchandise.def:60
msgid "USB-Sticks"
msgstr ""

#: ../../english/misc/merchandise.def:75
msgid "lanyards"
msgstr ""

#: ../../english/misc/merchandise.def:78
msgid "others"
msgstr ""

#: ../../english/misc/merchandise.def:98
msgid "Donates money to Debian"
msgstr ""

#: ../../english/misc/merchandise.def:102
msgid "Money is used to organize local free software events"
msgstr ""

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr ""

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr ""

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr ""

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr ""

#: ../../english/y2k/l10n.data:6
msgid "OK"
msgstr ""

#: ../../english/y2k/l10n.data:9
msgid "BAD"
msgstr ""

#: ../../english/y2k/l10n.data:12
msgid "OK?"
msgstr ""

#: ../../english/y2k/l10n.data:15
msgid "BAD?"
msgstr ""

#: ../../english/y2k/l10n.data:18
msgid "??"
msgstr ""

#: ../../english/y2k/l10n.data:21
msgid "Unknown"
msgstr ""

#: ../../english/y2k/l10n.data:24
msgid "ALL"
msgstr ""

#: ../../english/y2k/l10n.data:27
msgid "Package"
msgstr ""

#: ../../english/y2k/l10n.data:30
msgid "Status"
msgstr ""

#: ../../english/y2k/l10n.data:33
msgid "Version"
msgstr ""

#: ../../english/y2k/l10n.data:36
msgid "URL"
msgstr ""

#~ msgid "p<get-var page />"
#~ msgstr "str. <get-var page />"

#~ msgid "is a read-only, digestified version."
#~ msgstr "je "

#~ msgid "Subscription:"
#~ msgstr "Prijava:"

#~ msgid "Moderated:"
#~ msgstr "Moderiran:"

#~ msgid "No description given"
#~ msgstr "Ni opisa"

#~ msgid "Please select which lists you want to subscribe to:"
#~ msgstr "Izberite poxtne sezname, na katere se zxelite prijaviti"

#~ msgid ""
#~ "See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
#~ "how to subscribe using e-mail. An <a href=\"unsubscribe\">unsubscription "
#~ "web form</a> is also available, for unsubscribing from mailing lists. "
#~ msgstr ""
#~ "Za podatke o prijavljanu na posxtne sezname glejte stran <a href=\"./"
#~ "#subunsub\">posxtni seznami</a>. Na voljo je tudi <a href=\"unsubscribe"
#~ "\">spletni obrazec za odjavljanje</a> s posxtnih seznamov."

#~ msgid "Mailing List Subscription"
#~ msgstr "Prijava na posxtni seznam"
