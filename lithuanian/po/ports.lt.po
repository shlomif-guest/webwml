#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2009-02-28 18:23+0300\n"
"Last-Translator: Aleksandr Charkov <alcharkov@gmail.com>\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/ports/alpha/menu.inc:6
msgid "Debian for Alpha"
msgstr "Debian Alpha"

#: ../../english/ports/hppa/menu.inc:6
msgid "Debian for PA-RISC"
msgstr "Debian PA-RISC"

#: ../../english/ports/hurd/menu.inc:10
msgid "Hurd CDs"
msgstr "Hurd CD"

#: ../../english/ports/ia64/menu.inc:6
msgid "Debian for IA-64"
msgstr "Debian IA-64"

#: ../../english/ports/menu.defs:11
msgid "Contact"
msgstr "Kontaktas"

#: ../../english/ports/menu.defs:15
msgid "CPUs"
msgstr "CPU"

#: ../../english/ports/menu.defs:19
msgid "Credits"
msgstr "Padėka"

#: ../../english/ports/menu.defs:23
msgid "Development"
msgstr "Kūrimas"

#: ../../english/ports/menu.defs:27
msgid "Documentation"
msgstr "Dokumentacija"

#: ../../english/ports/menu.defs:31
msgid "Installation"
msgstr "Įdiegimas"

#: ../../english/ports/menu.defs:35
msgid "Configuration"
msgstr ""

#: ../../english/ports/menu.defs:39
msgid "Links"
msgstr "Nuorodos"

#: ../../english/ports/menu.defs:43
msgid "News"
msgstr "Naujienos"

#: ../../english/ports/menu.defs:47
msgid "Porting"
msgstr "Plėtojamas portatyvumas"

#: ../../english/ports/menu.defs:51
msgid "Ports"
msgstr "Perkėlimai"

#: ../../english/ports/menu.defs:55
msgid "Problems"
msgstr "Problemos"

#: ../../english/ports/menu.defs:59
msgid "Software Map"
msgstr "Programinės įrangos žemėlapis"

#: ../../english/ports/menu.defs:63
msgid "Status"
msgstr "Statusas"

#: ../../english/ports/menu.defs:67
msgid "Supply"
msgstr "Aprūpinimas"

#: ../../english/ports/menu.defs:71
msgid "Systems"
msgstr "Sistemos"

#: ../../english/ports/netbsd/menu.inc:6
msgid "Debian GNU/NetBSD for i386"
msgstr "Debian GNU/NetBSD i386"

#: ../../english/ports/netbsd/menu.inc:10
msgid "Debian GNU/NetBSD for Alpha"
msgstr "Debian GNU/NetBSD Alpha"

#: ../../english/ports/netbsd/menu.inc:14
msgid "Why"
msgstr "Kodėl"

#: ../../english/ports/netbsd/menu.inc:18
msgid "People"
msgstr "Žmonės"

#: ../../english/ports/powerpc/menu.inc:6
msgid "Debian for PowerPC"
msgstr "Debian PowerPC"

#: ../../english/ports/sparc/menu.inc:6
msgid "Debian for Sparc"
msgstr "Debian for Sparc"

#~ msgid "Debian for Laptops"
#~ msgstr "Debian nešiojamiems kompiuteriams"

#~ msgid "Debian for AMD64"
#~ msgstr "Debian AMD64"

#~ msgid "Debian for ARM"
#~ msgstr "Debian ARM"

#~ msgid "Debian for Beowulf"
#~ msgstr "Debian Beowulf"

#~ msgid "Main"
#~ msgstr "Pagrindinis"

#~ msgid "Debian GNU/FreeBSD"
#~ msgstr "Debian GNU/FreeBSD"

#~ msgid "Debian for Motorola 680x0"
#~ msgstr "Debian Motorola 680x0"

#~ msgid "Vendor/Name"
#~ msgstr "Pardavėjai/Pavadinimas"

#~ msgid "Date announced"
#~ msgstr "Paskelbimo datą"

#~ msgid "Clock"
#~ msgstr "Clock"

#~ msgid "ICache"
#~ msgstr "ICache"

#~ msgid "DCache"
#~ msgstr "DCache"

#~ msgid "TLB"
#~ msgstr "TLB"

#~ msgid "ISA"
#~ msgstr "ISA"

#~ msgid "Specials"
#~ msgstr "Specialūs"

#~ msgid "No FPU (R2010), external caches"
#~ msgstr "No FPU (R2010), external caches"

#~ msgid "No FPU (R3010)"
#~ msgstr "No FPU (R3010)"

#~ msgid "Virtual indexed L1 cache, L2 cache controller"
#~ msgstr "Virtual indexed L1 cache, L2 cache controller"

#~ msgid "External L1 cache"
#~ msgstr "External L1 cache"

#~ msgid "Multiple chip CPU"
#~ msgstr "Multiple chip CPU"

#~ msgid ""
#~ "Mips16 isa extension, No FPU, 512k flash, 16k ram, DMAC, UART, Timer, "
#~ "I2C, Watchdog"
#~ msgstr ""
#~ "Mips16 isa extension, No FPU, 512k flash, 16k ram, DMAC, UART, Timer, "
#~ "I2C, Watchdog"

#~ msgid "No FPU, DRAMC, ROMC, DMAC, UART, Timer, I2C, LCD Controller"
#~ msgstr "No FPU, DRAMC, ROMC, DMAC, UART, Timer, I2C, LCD Controller"

#~ msgid "No FPU, DRAMC, ROMC, DMAC, UART, Timer"
#~ msgstr "No FPU, DRAMC, ROMC, DMAC, UART, Timer"

#~ msgid "No FPU, DRAMC, ROMC, DMAC, PCIC, UART, Timer"
#~ msgstr "No FPU, DRAMC, ROMC, DMAC, PCIC, UART, Timer"

#~ msgid "No FPU, SDRAMC, ROMC, Timer, PCMCIA, LCD Controller, IrDA"
#~ msgstr "No FPU, SDRAMC, ROMC, Timer, PCMCIA, LCD Controller, IrDA"

#~ msgid "No FPU, SDRAMC, ROMC, UART, Timer, PCMCIA, LCD Controller, IrDA"
#~ msgstr "No FPU, SDRAMC, ROMC, UART, Timer, PCMCIA, LCD Controller, IrDA"

#~ msgid "No FPU, SDRAMC, ROMC, UART, Timer, PCMCIA, IrDA"
#~ msgstr "No FPU, SDRAMC, ROMC, UART, Timer, PCMCIA, IrDA"

#~ msgid "No FPU, SDRAMC, ROMC, DMAC, PCIC, UART, Timer"
#~ msgstr "No FPU, SDRAMC, ROMC, DMAC, PCIC, UART, Timer"

#~ msgid "2 10/100 Ethernet, 4 UART, IrDA, AC'97, I2C, 38 Digital I/O"
#~ msgstr "2 10/100 Ethernet, 4 UART, IrDA, AC'97, I2C, 38 Digital I/O"

#~ msgid "FPU, 32-bit external bus"
#~ msgstr "FPU, 32-bit external bus"

#~ msgid "FPU, 64-bit external bus"
#~ msgstr "FPU, 64-bit external bus"

#~ msgid "FPU, 64-bit external bus, external L2 cache"
#~ msgstr "FPU, 64-bit external bus, external L2 cache"

#~ msgid "256 L2 cache on die"
#~ msgstr "256 L2 cache on die"

#~ msgid "Mips 16"
#~ msgstr "Mips 16"

#~ msgid ""
#~ "Mips 16, RTC, Keyboard, TouchPanel, Audio, Compact-Flash, UART, Parallel"
#~ msgstr ""
#~ "Mips 16, RTC, Keyboard, TouchPanel, Audio, Compact-Flash, UART, Parallel"

#~ msgid "Mips 16, Compact Flash, UART, Parallel, RTC, Audio, PCIC"
#~ msgstr "Mips 16, Compact Flash, UART, Parallel, RTC, Audio, PCIC"

#~ msgid ""
#~ "Mips 16, LCD controller, Compact Flash, UART, Parallel, RTC, Keyboard, "
#~ "USB, Touchpad, Audio"
#~ msgstr ""
#~ "Mips 16, LCD controller, Compact Flash, UART, Parallel, RTC, Keyboard, "
#~ "USB, Touchpad, Audio"

#~ msgid "Debian for MIPS"
#~ msgstr "Debian MIPS"

#~ msgid "Debian for S/390"
#~ msgstr "Debian S/390"

#~ msgid "Debian for Sparc64"
#~ msgstr "Debian Sparc64"
