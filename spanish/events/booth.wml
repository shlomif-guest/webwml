#use wml::debian::template title="Atender un stand"
#use wml::debian::translation-check translation="2e3c0166e74b21c80857525f489ba5387969cc82" maintainer="Alfredo Quintero"
# $Id$

<p>La actividad más importante para los miembros del proyecto Debian 
que acuden a una exhibición es atender el puesto de Debian. Es bastante 
fácil ya que para montar un puesto basta con una máquina corriendo 
un sistema Debian GNU (da igual si Linux, Hurd o *BSD) y alguien dando
explicaciones de cómo funciona, y contestando preguntas. Sin embargo,
se recomienda que al menos estén presentes dos personas y el uso de
carteles por todos sitios con algunas máquinas más.
</p>

<p>Las exposiciones y conferencias suelen ser lugar apropiado para que
los desarrolladores se conozcan, incluso cuando viven lejos del lugar
donde se realiza.
A menudo los desarrolladores extranjeros son invitados para dar charlas,
así que incluso estas personas se pueden conocer allí. Además siempre está
bien ir a un bar con otros desarrolladores y hablar sobre cómo mejorar
el caché de inodos de linux o la resolución de dependencias de dpkg.
</p>

<p>En muchos casos ya se han creado páginas web y listas de correo 
para ayudar en la planificación de la presencia de Debian. Revise los
enlaces a &laquo;coordinación&raquo; en nuestras páginas de actos próximos y pasados
para hacerse una idea de cómo funciona esto. Lea también, por favor,
nuestra <a
href="checklist">lista de cosas para un stand</a>.
</p>


<h3><a name="ml">Listas de Correo</a></h3>

<p> El proyecto Debian proporciona listas de correo para coordinar 
la presencia de Debian en varios eventos:

<ul>
  <li> <a href="https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-eventos">debian-br-eventos</a>,
    dedicada a eventos en Brasil.
  <li> <a href="https://lists.debian.org/debian-dug-in/">debian-dug-in</a>,
	dedicada a eventos en la India.
  <li> <a href="https://lists.debian.org/debian-events-eu/">debian-events-eu</a>,
 	dedicada a Europa y los eventos europeos.
  <li> <a href="https://lists.debian.org/debian-events-ha/">debian-events-ha</a>,
    dedicada a eventos en Hispanoamérica.
  <li> <a href="https://lists.debian.org/debian-events-na/">debian-events-na</a>,
	dedicada a Norteamérica y los eventos en este continente.
  <li> <a href="https://lists.debian.org/debian-events-nl/">debian-events-nl</a>,
    dedicada a eventos en los Países Bajos.
</ul>

<p>Para suscribirse a cualquiera de estas listas, mire en la <a
href="$(HOME)/MailingLists/subscribe">página de suscripciones</a>.

