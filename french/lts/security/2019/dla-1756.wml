#use wml::debian::translation-check translation="57d82f6db850c200e5ba1fa26c165672c5fb3e6d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il existait une vulnérabilité de contournement d’authentification dans
libxslt, une bibliothèque largement utilisée pour transformer des fichiers d’XML
vers un autre format arbitraire.</p>

<p>Les routines xsltCheckRead et xsltCheckWrite permettait un accès lors de la
réception d’un code d’erreur -1 et (comme xsltCheckRead renvoyait -1 pour
une URL contrefaite pour l'occasion qui n’est pas en fait non valable) l’attaquant
était subséquemment authentifié.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11068">CVE-2019-11068</a>

<p>libxslt jusqu’à 1.1.33 permet le contournement du mécanisme de protection
parce les appelants de xsltCheckRead et xsltCheckWrite permettent l’accès même
lors de la réception d’un code d’erreur -1. xsltCheckRead peut renvoyer -1 pour
une URL contrefaite qui n’est pas en fait non valable et par la suite chargée.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.1.28-2+deb8u4.</p>
<p>Nous vous recommandons de mettre à jour vos paquets libxslt.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1756.data"
# $Id: $
