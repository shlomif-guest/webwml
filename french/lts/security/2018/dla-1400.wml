#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans le Tomcat
servlet moteur de JSP and.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7674">CVE-2017-7674</a>

<p>Le filtre CORS dans Apache Tomcat n'ajoutait pas d'en-tête « Vary » HTTP
indiquant que la réponse dépendait de l’origine. Cela pourrait conduire à
l'empoisonnement du cache côtés client et serveur dans certaines circonstances.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12616">CVE-2017-12616</a>

<p>Lors de l’utilisation de VirtualDirContext avec Tomcat d’Apache, il était
possible de contourner les contraintes de sécurité et/ou de voir le code source
de JSP pour des ressources servies par le VirtualDirContext en utilisant une
requête contrefaite pour l'occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1304">CVE-2018-1304</a>

<p>Le modèle d’URL de "" (la chaîne vide) qui correspond exactement au contexte
du superutilisateur, n’était pas géré correctement dans Tomcat d’Apache
lorsqu’utilisé comme partie de la définition de restriction. Cela faisait que la
restriction était ignorée. Il était, par conséquent, possible pour des
utilisateurs non autorisés d’acquérir l’accès aux ressources de l’application
web qui auraient dû être protégées. Seules les restrictions de sécurité avec
comme modèle d’URL la chaîne vide sont touchées.</p></li>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1305">CVE-2018-1305</a>

<p>Des restrictions de sécurité définies par des annotations de servlet dans
Tomcat d’Apache étaient seulement appliquées que lorsqu’un servlet était chargé.
À cause des restrictions de sécurité définies de cette façon appliquées au modèle
d’URL et n’importe quelle URL en découlant, il était possible — en fonction
de l’ordre de chargement des servlets — que quelques restrictions de sécurité
ne soient appliquées. Cela pourrait avoir exposé des ressources à des
utilisateurs n’ayant pas de droit d’accès.</p></li>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8014">CVE-2018-8014</a>

<p>Les réglages par défaut pour le filtre CORS fourni dans Tomcat d’Apache ne
sont par sûrs et autorisent <q>supportsCredentials</q> pour toutes les origines.
Il était prévu que les utilisateurs du filtre CORS devaient l’avoir configuré de
manière appropriée à leur environnement plutôt que de l’utiliser avec la
configuration par défaut. Par conséquent, il est espéré que la plupart des
utilisateurs n’étaient pas impactés par ce problème.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 7.0.56-3+really7.0.88-1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets tomcat7.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1400.data"
# $Id: $
