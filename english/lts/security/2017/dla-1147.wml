<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The exiv2 library is vulnerable to multiple issues that can all lead
to denial of service of the applications relying on the library to parse
images' metadata.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11591">CVE-2017-11591</a>

    <p>Denial of service via floating point exception in
    the Exiv2::ValueType function.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11683">CVE-2017-11683</a>

    <p>Denial of service through failing assertion triggered by
    crafted image.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14859">CVE-2017-14859</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-14862">CVE-2017-14862</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-14864">CVE-2017-14864</a>

    <p>Denial of service through invalid memory access triggered by a crafted
    image.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.23-1+deb7u2.</p>

<p>We recommend that you upgrade your exiv2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1147.data"
# $Id: $
