<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in phpMyAdmin, a tool to
administer MySQL over the web. The Common Vulnerabilities and Exposures
project identifies the following problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-3239">CVE-2013-3239</a>

    <p>Authenticated users could execute arbitrary code, when a SaveDir
    directory is configured and Apache HTTP Server has the mod_mime
    module enabled, by employing double filename extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-4995">CVE-2013-4995</a>

    <p>Authenticatd users could inject arbitrary web script or HTML
    via a crafted SQL query.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-4996">CVE-2013-4996</a>

    <p>Cross site scripting was possible via a crafted logo URL in
    the navigation panel or a crafted entry in the Trusted Proxy list.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-5003">CVE-2013-5003</a>

    <p>Authenticated users could execute arbitrary SQL commands as
    the phpMyAdmin <q>control user</q> via the scale parameter of PMD PDF
    export.</p>

</ul>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in phpmyadmin version 4:3.3.7-8</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2014/dla-0014.data"
# $Id: $
