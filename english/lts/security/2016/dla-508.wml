<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two related issues have been discovered in Expat, a C library for
parsing XML.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2012-6702">CVE-2012-6702</a>

    <p>This issue was introduced when <a href="https://security-tracker.debian.org/tracker/CVE-2012-0876">CVE-2012-0876</a> was addressed. Stefan
    Sørensen discovered that the use of the function XML_Parse() seeds
    the random number generator generating repeated outputs for rand()
    calls.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5300">CVE-2016-5300</a>

    <p>This is the product of an incomplete solution for <a href="https://security-tracker.debian.org/tracker/CVE-2012-0876">CVE-2012-0876</a>. The
    parser poorly seeds the random number generator allowing an
    attacker to cause a denial of service (CPU consumption) via an XML
    file with crafted identifiers.</p></li>

</ul>

<p>You might need to manually restart programs and services using expat
libraries.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.1.0-1+deb7u4.</p>

<p>We recommend that you upgrade your expat packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-508.data"
# $Id: $
